\documentclass[10pt, a4paper]{article}
\usepackage[colorlinks,linkcolor=blue,urlcolor=blue,citecolor=blue]{hyperref}
\usepackage{mathtools}
\usepackage[capitalize]{cleveref}
\usepackage[nolist]{acronym}

\title{Annual activity report}

\author{Ryan Moodie}

\date{23 March 2023}

\begin{document}

\begin{acronym}
    \acro{LHC}{Large Hadron Collider}
    \acro{N3LO}[N\textsuperscript{3}LO]{(next-to-)\textsuperscript{3}leading order}
    \acro{NNLO}{next-to-next-to-leading order}
    \acro{NN}{neural network}
    \acro{QCD}{quantum chromodynamics}
    \acro{QED}{quantum electrodynamics}
\end{acronym}

\maketitle

\begin{abstract}
Since starting here in May 2022, I have worked in Simon Badger's group on the calculation of higher-order corrections in \ac{QCD} and \ac{QED} to Standard Model amplitudes relevant for precision phenomenology experiments such as those at the \ac{LHC}.
I have focussed on using novel technologies such as analytic reconstruction over finite fields and deep \acp{NN} to perform state-of-the-art computations.
\end{abstract}

\section*{Projects}
\label{sec:projects}

\begin{itemize}
    \item We are computing the \ac{NNLO} \ac{QCD} corrections to the production of an isolated photon in association with a pair of jets at hadron colliders, $pp\to\gamma jj$.
        We have calculated the double-virtual corrections, which are massless five-particle two-loop amplitudes, in full-colour.
        We used reconstruction over finite fields~\cite{Badger:2021imn} to obtain analytic expressions for the rational coefficients, and used the pentagon functions~\cite{Chicherin:2021dyp} for a special function basis.
        Our collaborators are using these amplitude ingredients to compute \ac{NNLO} distributions to demonstrate their importance for precision phenomenology.

    \item We are calculating the \ac{N3LO} real-double-virtual \ac{QED} corrections to the decay of an off-shell photon to a lepton pair, $\gamma^*\to\ell\bar\ell$, in the approximation of massless leptons.
        This includes the two-loop amplitudes for the channel $0\to\ell\bar\ell\gamma\gamma^*$, a four-particle process with one external massive leg.
        Having found them through the Laporta algorithm, we are solving the Master Integrals using the method of differential equations and will express them in terms of Goncharov Polylogarithms~\cite{Zoia2022}.
        We are again using reconstruction over finite fields for the rational coefficients.
        These amplitudes will be used by the McMule collaboration for \ac{N3LO} \ac{QED} $\gamma^*\to\ell\bar\ell$ computations relevant for low-energy experiments~\cite{Ulrich:2020frs}.

    \item We are investigating the use of machine learning technology to optimise several avenues of our calculations.
        We previously found \acp{NN} to be a promising surrogate for high-multiplicity amplitudes~\cite{Aylett-Bullock:2021hmo}, and there is certainly scope to progress in this direction.
        With the evaluation of Feynman integrals being a bottleneck in the computation of amplitudes, we have also been studying whether \acp{NN}, as universal function approximators, trained on differential equations~\cite{Lagaris:1997at} could provide an efficient surrogate or allow the evaluation of currently inaccessible integrals.
\end{itemize}

\section*{Papers}
\label{sec:papers}

\begin{itemize}
    \item My PhD thesis was accepted~\cite{Moodie:2022dxs}.
    \item The proceedings contribution for my talk at the \textit{20\textsuperscript{st} International Workshop on Advanced Computing and Analysis Techniques in Physics Research} (ACAT 2021) was published~\cite{Moodie:2022flt}.
    \item I submitted a proceedings contribution for my talk at the \textit{21\textsuperscript{st} International Workshop on Advanced Computing and Analysis Techniques in Physics Research} (ACAT 2022) and released the preprint~\cite{Moodie:2023pql}.
\end{itemize}

\section*{Talks}
\label{sec:talks}

\begin{itemize}
    \item On 9 June 2022, I gave a seminar titled \textit{Optimising hadron collider simulations using amplitude neural networks}~\cite{moodie_ryan_2022_7759561} to the Theoretical Physics II group at the Julius-Maximilians-Universität of Würzburg, Germany.

    \item On 3--5 August 2022, I attended the \textit{4\textsuperscript{th} Workstop/Thinkstart: Towards \acs{N3LO} for $\gamma^*\to\ell\bar\ell$} in Durham, UK.
        I gave a talk titled \textit{Analytic two-loop amplitudes with finite fields}~\cite{moodie_ryan_2022_7759592}.

    \item On 20--22 September 2022, I attended the \textit{8\textsuperscript{th} International Workshop on High Precision for Hard Processes at the \acs{LHC}} (HP$^2$ 2022) in Newcastle, UK.
        I gave a talk titled \textit{Two-loop five-point amplitudes in massless \acs{QCD} with finite fields}~\cite{moodie_ryan_2022_7759875}.

    \item On 23--28 October 2022, I attended the \textit{21\textsuperscript{st} International Workshop on Advanced Computing and Analysis Techniques in Physics Research} (ACAT 2022) in Bari, Italy.
        I gave a talk titled \textit{Two-loop five-point amplitudes in massless \acs{QCD} with finite fields}~\cite{moodie_ryan_2022_7760420}.

    \item On 7 November 2022, I virtually gave a seminar titled \textit{Optimising hadron collider simulations using matrix element neural networks}~\cite{moodie_ryan_2022_7760525} at a CMS Physics Generators meeting in CERN.
\end{itemize}

\bibliographystyle{JHEPedit}
\bibliography{bibliography}

\end{document}
